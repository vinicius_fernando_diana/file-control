
package br.com.fourward.filecontrol.data;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "merchant")
public class MerchantEntity implements Serializable {

	private static final long serialVersionUID = 7239368384699225565L;

	private static final String GENERATOR_NAME = "SQ_MERCHANT_ID";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
	@SequenceGenerator(name = GENERATOR_NAME, sequenceName = GENERATOR_NAME, allocationSize = 1)
	@Column
	private Long id;

	@Column
	private String legalName;

	@Column
	private String socialName;

	@Column
	private Boolean allowedToTransact;

	@Column(updatable = false)
	private LocalDate foundation;

	@Column
	private String document;

	@Column
	private Boolean isActive;

	public MerchantEntity() {
		super();
	}

	public MerchantEntity(Long id,
	        String legalName,
	        String socialName,
	        Boolean allowedToTransact,
	        LocalDate foundation,
	        String document,
	        Boolean isActive) {
		super();
		this.id = id;
		this.legalName = legalName;
		this.socialName = socialName;
		this.allowedToTransact = allowedToTransact;
		this.foundation = foundation;
		this.document = document;
		this.isActive = isActive;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getSocialName() {
		return socialName;
	}

	public void setSocialName(String socialName) {
		this.socialName = socialName;
	}

	public Boolean getAllowedToTransact() {
		return allowedToTransact;
	}

	public void setAllowedToTransact(Boolean allowedToTransact) {
		this.allowedToTransact = allowedToTransact;
	}

	public LocalDate getFoundation() {
		return foundation;
	}

	public void setFoundation(LocalDate foundation) {
		this.foundation = foundation;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allowedToTransact == null) ? 0 : allowedToTransact.hashCode());
		result = prime * result + ((document == null) ? 0 : document.hashCode());
		result = prime * result + ((foundation == null) ? 0 : foundation.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isActive == null) ? 0 : isActive.hashCode());
		result = prime * result + ((legalName == null) ? 0 : legalName.hashCode());
		result = prime * result + ((socialName == null) ? 0 : socialName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MerchantEntity other = (MerchantEntity) obj;
		if (allowedToTransact == null) {
			if (other.allowedToTransact != null)
				return false;
		} else if (!allowedToTransact.equals(other.allowedToTransact))
			return false;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (foundation == null) {
			if (other.foundation != null)
				return false;
		} else if (!foundation.equals(other.foundation))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isActive == null) {
			if (other.isActive != null)
				return false;
		} else if (!isActive.equals(other.isActive))
			return false;
		if (legalName == null) {
			if (other.legalName != null)
				return false;
		} else if (!legalName.equals(other.legalName))
			return false;
		if (socialName == null) {
			if (other.socialName != null)
				return false;
		} else if (!socialName.equals(other.socialName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MerchantEntity [id="
		        + id
		        + ", legalName="
		        + legalName
		        + ", socialName="
		        + socialName
		        + ", allowedToTransact="
		        + allowedToTransact
		        + ", foundation="
		        + foundation
		        + ", document="
		        + document
		        + ", isActive="
		        + isActive
		        + "]";
	}
}
