
package br.com.fourward.filecontrol.dto;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class MerchantDto implements Serializable {

	private static final long serialVersionUID = 7239368384699225565L;

	private Long id;

	private String legalName;

	private String socialName;

	private Boolean allowedToTransact;

	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, locale = "pt-BR", timezone = "Brazil/East")
	private LocalDate foundation;

	private DocumentDto document;

	private Boolean isActive;

	public MerchantDto() {
		super();
	}

	public MerchantDto(Long id,
	        String legalName,
	        String socialName,
	        Boolean allowedToTransact,
	        LocalDate foundation,
	        DocumentDto document,
	        Boolean isActive) {
		super();
		this.id = id;
		this.legalName = legalName;
		this.socialName = socialName;
		this.allowedToTransact = allowedToTransact;
		this.foundation = foundation;
		this.document = document;
		this.isActive = isActive;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getSocialName() {
		return socialName;
	}

	public void setSocialName(String socialName) {
		this.socialName = socialName;
	}

	public Boolean getAllowedToTransact() {
		return allowedToTransact;
	}

	public void setAllowedToTransact(Boolean allowedToTransact) {
		this.allowedToTransact = allowedToTransact;
	}

	public LocalDate getFoundation() {
		return foundation;
	}

	public void setFoundation(LocalDate foundation) {
		this.foundation = foundation;
	}

	public DocumentDto getDocument() {
		return document;
	}

	public void setDocument(DocumentDto document) {
		this.document = document;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allowedToTransact == null) ? 0 : allowedToTransact.hashCode());
		result = prime * result + ((document == null) ? 0 : document.hashCode());
		result = prime * result + ((foundation == null) ? 0 : foundation.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isActive == null) ? 0 : isActive.hashCode());
		result = prime * result + ((legalName == null) ? 0 : legalName.hashCode());
		result = prime * result + ((socialName == null) ? 0 : socialName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MerchantDto other = (MerchantDto) obj;
		if (allowedToTransact == null) {
			if (other.allowedToTransact != null)
				return false;
		} else if (!allowedToTransact.equals(other.allowedToTransact))
			return false;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (foundation == null) {
			if (other.foundation != null)
				return false;
		} else if (!foundation.equals(other.foundation))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isActive == null) {
			if (other.isActive != null)
				return false;
		} else if (!isActive.equals(other.isActive))
			return false;
		if (legalName == null) {
			if (other.legalName != null)
				return false;
		} else if (!legalName.equals(other.legalName))
			return false;
		if (socialName == null) {
			if (other.socialName != null)
				return false;
		} else if (!socialName.equals(other.socialName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MerchantDto [id="
		        + id
		        + ", legalName="
		        + legalName
		        + ", socialName="
		        + socialName
		        + ", allowedToTransact="
		        + allowedToTransact
		        + ", foundation="
		        + foundation
		        + ", document="
		        + document
		        + ", isActive="
		        + isActive
		        + "]";
	}
}
