
package br.com.fourward.filecontrol.business;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.fourward.FourwardLogger;
import br.com.fourward.filecontrol.canonical.MerchantCanonical;
import br.com.fourward.filecontrol.converter.tocanonical.ToMerchantCanonicalConverter;
import br.com.fourward.filecontrol.converter.toentity.ToMerchantEntityConverter;
import br.com.fourward.filecontrol.data.MerchantEntity;
import br.com.fourward.filecontrol.data.builder.MerchantEntityBuilder;
import br.com.fourward.filecontrol.data.repository.MerchantRepository;
import br.com.fourward.filecontrol.data.specification.MerchantSpecification;
import br.com.fourward.filecontrol.util.Pagination;

@Service
@FourwardLogger
public class MerchantBusiness {

	@Autowired
	private MerchantRepository merchantRepository;

	public MerchantBusiness() {}

	public MerchantCanonical save(final MerchantCanonical merchantCanonical) {
		if (LocalDate.now().isBefore(merchantCanonical.getFoundation())) {
			throw new RuntimeException("Merchant");
		}

		final MerchantEntity entity = convertCanonicalToEntity(merchantCanonical);
		final MerchantEntity savedEntity = merchantRepository.save(entity);

		return convertEntityToCanonical(savedEntity);
	}

	public MerchantCanonical getById(final Long id) {
		final MerchantEntity foundEntity = merchantRepository.findById(id)
		        .orElseThrow(() -> new RuntimeException("Merchant não encontrado: " + id));

		return convertEntityToCanonical(foundEntity);
	}

	public Pagination<MerchantCanonical> getAllPaginated(final String name, final int page, final int size) {
		final Pageable pageable = PageRequest.of(page, size);

		final MerchantEntity merchantEntity = MerchantEntityBuilder.create().setLegalName(name).build();
		final Example<MerchantEntity> example = Example.of(merchantEntity);

		// final Page<MerchantEntity> foundPage = merchantRepository.findAll(buildSpecification(name), pageable);
		final Page<MerchantEntity> foundPage = merchantRepository.findAll(example, pageable);

		final Pagination<MerchantEntity> pagination =
		        new Pagination<>(foundPage.getContent(), page, size, foundPage.getTotalElements());

		return pagination.map(this::convertEntityToCanonical);
	}

	public void deleteById(final Long id) {
		// FIXME ao invés de deletar, vamos apenas desativar.
	}

	private Specification<MerchantEntity> buildSpecification(final String name) {
		return Specification //
		        .where(Optional.ofNullable(name)
		                .map(legalName -> MerchantSpecification.specByLegalName(legalName))
		                .orElse(null))
		        .and(null)
		        .and(null)
		        .and(null);
	}

	private MerchantEntity convertCanonicalToEntity(final MerchantCanonical merchantCanonical) {
		return ToMerchantEntityConverter.convert(merchantCanonical);
	}

	private MerchantCanonical convertEntityToCanonical(final MerchantEntity merchantEntity) {
		return ToMerchantCanonicalConverter.convert(merchantEntity);
	}
}
