
package br.com.fourward.filecontrol.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.fourward.LoggingHandler;

@Configuration
public class FourwardLoggerConfiguration {

	@Bean
	public LoggingHandler getLogger() {
		return new LoggingHandler();
	}
}
