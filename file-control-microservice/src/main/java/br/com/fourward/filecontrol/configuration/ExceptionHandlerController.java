
package br.com.fourward.filecontrol.configuration;

import java.util.NoSuchElementException;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<?> NoSuchObjectExceptionHandler() {
		return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Oi");
	}

	@ExceptionHandler(NoSuchElementException.class)
	public ResponseEntity<?> NoSuchElementExceptionHandler() {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Nenhum resultado para a consulta");
	}

	// @ExceptionHandler(DataValidationException.class)
	// public ResponseEntity<?> DataValidationExceptionHandler(final DataValidationException e) {
	// final Error error = new Error(new Date(), "400", HttpStatus.BAD_REQUEST.name(), e.getMessage(), null);
	// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
	// }

	// @ExceptionHandler(MailException.class)
	// public ResponseEntity<?> MailExceptionHandler(final MailException e) {
	// final Error error = new Error(new Date(), "400", HttpStatus.BAD_REQUEST.name(), e.getMessage(), null);
	// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
	// }

	@ExceptionHandler(RollbackException.class)
	public ResponseEntity<?> ConstraintViolationExceptionHandler(final RollbackException ex) {
		if (ex.getCause() instanceof ConstraintViolationException) {
			final StringBuffer sb = new StringBuffer();
			for (final ConstraintViolation<?> constraint : ((ConstraintViolationException) ex.getCause())
			        .getConstraintViolations()) {
				sb.append(constraint.getMessage() + "\n");
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(sb.toString());
		} else {
			ex.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ocorreu um erro não esperado.");
		}
	}
}
