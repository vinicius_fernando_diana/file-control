
package br.com.fourward.filecontrol.converter.tocanonical;

import br.com.fourward.filecontrol.canonical.DocumentCanonical;
import br.com.fourward.filecontrol.canonical.DocumentType;
import br.com.fourward.filecontrol.canonical.builder.DocumentCanonicalBuilder;
import br.com.fourward.filecontrol.dto.DocumentDto;

public class ToDocumentCanonicalConverter {

	public static DocumentCanonical convert(final DocumentDto target) {
		final boolean targetIsNull = target == null;

		final Long id = targetIsNull ? null : target.getId();
		final String value = targetIsNull ? null : target.getValue();

		final boolean isTypeNull = targetIsNull || target.getType() == null;
		final DocumentType type = isTypeNull ? null : DocumentType.fromString(target.getType());

		return DocumentCanonicalBuilder //
		        .create() //
		        .setId(id) //
		        .setValue(value) //
		        .setType(type) //
		        .build();
	}
}
