
package br.com.fourward.filecontrol.converter.todto;

import br.com.fourward.filecontrol.canonical.DocumentCanonical;
import br.com.fourward.filecontrol.dto.DocumentDto;
import br.com.fourward.filecontrol.dto.builder.DocumentDtoBuilder;

public class ToDocumentDtoConverter {

	public static DocumentDto convert(final DocumentCanonical target) {
		final boolean targetIsNull = target == null;

		final Long id = targetIsNull ? null : target.getId();
		final String value = targetIsNull ? null : target.getValue();

		final boolean isTypeNull = targetIsNull || target.getType() == null;
		final String type = isTypeNull ? null : target.getType().getValue();

		return DocumentDtoBuilder //
		        .create() //
		        .setId(id) //
		        .setValue(value) //
		        .setType(type) //
		        .build();
	}
}
