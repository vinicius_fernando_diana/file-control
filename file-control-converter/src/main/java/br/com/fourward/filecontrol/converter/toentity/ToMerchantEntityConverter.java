
package br.com.fourward.filecontrol.converter.toentity;

import java.time.LocalDate;

import br.com.fourward.filecontrol.canonical.MerchantCanonical;
import br.com.fourward.filecontrol.data.MerchantEntity;
import br.com.fourward.filecontrol.data.builder.MerchantEntityBuilder;

public class ToMerchantEntityConverter {

	public static MerchantEntity convert(final MerchantCanonical target) {
		final boolean targetIsNull = target == null;

		final Long id = targetIsNull ? null : target.getId();
		final String legalName = targetIsNull ? null : target.getLegalName();
		final String socialName = targetIsNull ? null : target.getSocialName();
		final Boolean allowedToTransact = targetIsNull ? null : target.getAllowedToTransact();
		final LocalDate foundation = targetIsNull ? null : target.getFoundation();
		final Boolean isActive = targetIsNull ? null : target.getIsActive();

		final boolean isDocumentNull = targetIsNull || target.getDocument() == null;
		final String document = isDocumentNull ? null : target.getDocument().getValue();

		return MerchantEntityBuilder //
		        .create() //
		        .setId(id) //
		        .setLegalName(legalName) //
		        .setSocialName(socialName) //
		        .setAllowedToTransact(allowedToTransact) //
		        .setFoundation(foundation) //
		        .setDocument(document) //
		        .setIsActive(isActive) //
		        .build();
	}
}
